## Netplan

#### Force netplan to use Mac address for dhcp requests

network:
    renderer: networkd
    version: 2
    ethernets:
        nicdevicename:
            dhcp4: true
            dhcp-identifier: mac

(where "nicdevicename" is the appropriate device name)

`sudo netplan apply`

Source: https://superuser.com/questions/1338510/wrong-ip-address-from-dhcp-client-on-ubuntu-18-04
