## Retropie

###### Force HDMI every boot
  Edit `/boot/config.txt`:
  Uncomment these lines:
    `hdmi_force_hotplug=1`
    `hdmi_drive=2`

##### Fix "timed out" error when fsck runs on a very large volume
Edit /etc/fstab, add the following mount option to the root filesystem:
  `x-systemd.device-timeout=120`

###### Steps for setting up a fresh Retropie
  * Choose localization options (set WiFi country, locale, keyboard layout, timezone)
  * Disable overscan
  * Enable SSH
  * Set hostname
  * Install ntp, vim-nox
  * Enable HDMI fix (above)
  * Change ls alias to include -lh options
  * Rename amiga folder so it can't be found
  * Install Syncthing repo key:
      curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
  * Install Syncthing repo:
      echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
  * Install syncthing
  * Change megadrive branding to genesis:
      Edit/create /opt/retropie/configs/all/platforms.cfg
      Add:
        megadrive_theme="genesis"
        megadrive_platform="genesis"
  * Disable run menu
  * Edit /opt/retropie/configs/all/retroarch.cfg and update save paths to:
      savefile_directory = /home/pi/RetroPie/saves/srm
      savestate_directory = /home/pi/RetroPie/saves/state
  * Create the directories from the previous step
  * Copy over BIOS files
  * Copy over game lists and images if desired
  * Configure MAME roms for free play, correct controls, etc
